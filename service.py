# -*- coding: utf-8 -*-

""" Service Sleep Timer  (c)  2015 enen92, Solo0815

# This program is free software; you can redistribute it and/or modify it under the terms
# of the GNU General Public License as published by the Free Software Foundation;
# either version 2 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program;
# if not, see <http://www.gnu.org/licenses/>.


"""

import xbmc
import xbmcplugin
import xbmcgui
import xbmcaddon


msgdialogprogress = xbmcgui.DialogProgress()

addon_id = 'service.sleep.playback'
selfAddon = xbmcaddon.Addon(addon_id)
debug = selfAddon.getSetting('debug_mode')

__version__ = selfAddon.getAddonInfo('version')
check_time = selfAddon.getSetting('check_time')
time_to_wait = int(selfAddon.getSetting('waiting_time_dialog'))

video_enable = str(selfAddon.getSetting('video_enable'))
max_time_video = int(selfAddon.getSetting('max_time_video'))
enable_screensaver = selfAddon.getSetting('enable_screensaver')

# Functions:


def translate(text):
    return selfAddon.getLocalizedString(text).encode('utf-8')


def _log(message):
    xbmc.log('## sleep timer Addon ## ' + message, 4)

# wait for abort - xbmc.sleep or time.sleep doesn't work
# and prevents Kodi from exiting


def do_next_check(iTimeToWait):
    if debug == 'true':
        _log("DEBUG: next check in " + str(iTimeToWait) + " min")
    if xbmc.Monitor().waitForAbort(int(iTimeToWait)*60):
        exit()


class service:
    def __init__(self):
        FirstCycle = True
        next_check = False

        while True:
            if True:
                if FirstCycle:
                    enable_video = video_enable
                    maxvideo_time_in_minutes = max_time_video
                    iCheckTime = check_time

                    _log("started ... (" + str(__version__) + ")")
                    if debug == 'true':
                        _log(
                            "DEBUG: ################################################################")
                        _log("DEBUG: Settings in Kodi:")
                        _log("DEBUG: enable_video: " + str(enable_video))
                        _log("DEBUG: maxvideo_time_in_minutes: " +
                             str(maxvideo_time_in_minutes))
                        _log("DEBUG: check_time: " + str(iCheckTime))
                        _log("DEBUG: Supervision mode: Always")
                        _log(
                            "DEBUG: ################################################################")
                        # Set this low values for easier debugging!
                        _log("DEBUG: debug is enabled! Override Settings:")
                        maxvideo_time_in_minutes = 1
                        _log("DEBUG: -> maxvideo_time_in_minutes: " +
                             str(maxvideo_time_in_minutes))
                        iCheckTime = 1
                        _log("DEBUG: -> check_time: " + str(iCheckTime))
                        _log(
                            "DEBUG: ----------------------------------------------------------------")

                    # wait 15s before start to let Kodi finish the intro-movie
                    if xbmc.Monitor().waitForAbort(15):
                        break

                    max_time_in_minutes = -1
                    FirstCycle = False

                idle_time = xbmc.getGlobalIdleTime()
                idle_time_in_minutes = int(idle_time)/60

                if xbmc.Player().isPlaying():

                    if debug == 'true' and max_time_in_minutes == -1:
                        _log(
                            "DEBUG: max_time_in_minutes before calculation: " + str(max_time_in_minutes))

                    if next_check == 'true':
                        # add "diff_between_idle_and_check_time" to "idle_time_in_minutes"
                        idle_time_in_minutes += int(
                            diff_between_idle_and_check_time)

                    if debug == 'true' and max_time_in_minutes == -1:
                        _log(
                            "DEBUG: max_time_in_minutes after calculation: " + str(max_time_in_minutes))

                    if xbmc.Player().isPlayingVideo():
                        if enable_video == 'true':
                            if debug == 'true':
                                _log("DEBUG: enable_video is true")

                            what_is_playing = "video"
                            max_time_in_minutes = maxvideo_time_in_minutes
                        else:
                            if debug == 'true':
                                _log(
                                    "DEBUG: Player is playing Video, but check is disabled")
                            do_next_check(iCheckTime)
                            continue

                    ### ToDo:
                    # expand it with RetroPlayer for playing Games!!!

                    else:
                        if debug == 'true':
                            _log("DEBUG: Player is playing, but no Audio or Video")
                        what_is_playing = "other"
                        do_next_check(iCheckTime)
                        continue

                    if debug == 'true':
                        _log("DEBUG: what_is_playing: " + str(what_is_playing))

                    if debug == 'true':
                        _log("DEBUG: idle_time: '" + str(idle_time) +
                             "s'; idle_time_in_minutes: '" + str(idle_time_in_minutes) + "'")
                        _log("DEBUG: max_time_in_minutes: " +
                             str(max_time_in_minutes))

                    # only display the Progressdialog, if audio or video is enabled AND idle limit is reached

                    # Check if what_is_playing is not "other" and idle time exceeds limit
                    if (what_is_playing != "other" and idle_time_in_minutes >= max_time_in_minutes):

                        if debug == 'true':
                            _log(
                                "DEBUG: idle_time exceeds max allowed. Display Progressdialog")

                        ret = msgdialogprogress.create(
                            translate(30000), translate(30001))
                        secs = 0
                        percent = 0
                        # use the multiplier 100 to get better %/calculation
                        increment = 100*100 / time_to_wait
                        cancelled = False
                        while secs < time_to_wait:
                            secs = secs + 1
                            # divide with 100, to get the right value
                            percent = increment*secs/100
                            secs_left = str((time_to_wait - secs))
                            remaining_display = str(
                                secs_left) + " " + translate(30029)
                            msgdialogprogress.update(
                                percent, translate(30001), remaining_display)
                            xbmc.sleep(1000)
                            if (msgdialogprogress.iscanceled()):
                                cancelled = True
                                if debug == 'true':
                                    _log("DEBUG: Progressdialog cancelled")
                                break
                        if cancelled == True:
                            iCheckTime = check_time
                            _log("Progressdialog cancelled, next check in " +
                                 str(iCheckTime) + " min")
                            # set next_check, so that it opens the dialog after "iCheckTime"
                            next_check = True
                            msgdialogprogress.close()
                        else:
                            _log("Progressdialog not cancelled: stopping Player")
                            msgdialogprogress.close()

                            xbmc.executebuiltin('PlayerControl(Stop)')

                            if enable_screensaver == 'true':
                                if debug == 'true':
                                    _log("DEBUG: Activating screensaver")
                                xbmc.executebuiltin('ActivateScreensaver')

                    else:
                        if debug == 'true':
                            _log(
                                "DEBUG: Playing the stream, time does not exceed max limit")
                else:
                    if debug == 'true':
                        _log("DEBUG: Not playing any media file")
                    # reset max_time_in_minutes
                    max_time_in_minutes = -1

                diff_between_idle_and_check_time = idle_time_in_minutes - \
                    int(iCheckTime)

                if debug == 'true' and next_check == 'true':
                    _log("DEBUG: diff_between_idle_and_check_time: " +
                         str(diff_between_idle_and_check_time))

                do_next_check(iCheckTime)


service()
